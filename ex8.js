//For this challenge you will determine if two characters are separated a specific way in the string.

function coincidedWay(str){

    const firstCoincided = /a...b/;
    const secondCoincided = /b...a/;

    const aB = firstCoincided.test(str);
    const bA = secondCoincided.test(str);

  return aB || bA;
}

console.log(coincidedWay(""));