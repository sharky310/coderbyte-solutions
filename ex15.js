// Have the function ArrayAdditionI(arr) take the array of numbers stored in arr and return the string true if any combination of numbers in the array (excluding the largest number) can be added up to equal the largest number in the array, otherwise return the string false.

function ArrayAdditionI(arr) { 

    let maxPos=0;
    let acum=0;

    for (const x in arr) {
        if (arr[maxPos]<arr[x]) maxPos = x;
    }
    
    for (const x in arr) {
        if (x != maxPos) acum += arr[x];
    }

    return arr[maxPos] < acum;
  }

  // keep this function call here 
let box = [1,2,3,4,25];

console.log(ArrayAdditionI(box));     