//For this challenge you will be sorting characters in a string.
//Input:"coderbyte"
//Output:bcdeeorty

function reOrder(str){

    return str.split("").sort().join("");
}

console.log(reOrder("coderbyte"));