// Realiza a function that change the first letter in a word and capitalized it
function LetterCapitalize(str){

  str = str.split(" ");

  for (let i = 0, x = str.length; i < x; i++) {
      str[i] = str[i][0].toUpperCase() + str[i].substr(1);
  }

  return str.join(" ");
}

let test="hello world";
console.log(LetterCapitalize(test));
